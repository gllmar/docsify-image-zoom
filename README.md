# [docsify-image-zoom](https://gllmar.gitlab.io/docsify-lightbox/#/)



## Docsify lightbox

This plugin enhances the Docsify image zoom functionality with additional features such as navigation between images, touch screen interaction, and better handling of high-resolution images.
Features

* Image Zoom: Click on an image to zoom in.
* High-Resolution Images: Displays high-resolution versions of images when zoomed in.
* Navigation: Use arrow keys or navigation buttons to move between images.
* Touch Screen Support: Swipe left or right to navigate between images on touch screen devices.
* Exit Button: Intuitively placed exit button to close the zoomed image.
* Captions: Displays alt text as a caption at the bottom center of the zoomed image.
* Smooth Transitions: Ensures smooth transitions and animations for a better user experience.

## Installation

Include the plugin script in your Docsify setup:

```html
<script src="https://gllmar.gitlab.io/docsify-lightbox/docsify-lightbox.js"></script>
```


## Usage

* Zoom an Image: Click on an image to zoom in.
* Navigate Images: Use the left and right arrow keys, swipe left or right, or click the navigation buttons to move between images.
* Close Zoomed Image: Click the exit button or press the Escape key to close the zoomed image.

## Customization

You can customize the plugin by modifying the JavaScript and CSS code according to your needs. The provided CSS classes can be styled as desired.
CSS Classes

* .medium-zoom-overlay: The overlay background when an image is zoomed.
* .medium-zoom-image: The class added to images that can be zoomed.
* .medium-zoom-image--hidden: The class added to the original image when zoomed.
* .medium-zoom-image--opened: The class added to the zoomed image.
* .medium-zoom-nav-button: The class for navigation buttons.
* .medium-zoom-nav-left: The class for the left navigation button.
* .medium-zoom-nav-right: The class for the right navigation button.
* .medium-zoom-exit-button: The class for the exit button.
* .medium-zoom-caption: The class for the image caption.

## Contributing

Feel free to contribute by opening issues or submitting pull requests 

## License

This plugin is open-sourced software licensed under the MIT license.

## Example implementation  

### Images in a list

* ![A vibrant futuristic cityscape at sunset.](./media/DALL·E%202024-05-18%2010.48.30%20-%20A%20vibrant,%20futuristic%20cityscape%20at%20sunset%20with%20neon%20lights,%20diverse%20architecture,%20and%20flying%20cars%20in%20the%20sky.%20The%20buildings%20have%20various%20shapes%20and%20si.webp)
* ![A surreal underwater scene.](./media/DALL·E%202024-05-18%2010.49.04%20-%20A%20surreal%20underwater%20scene%20with%20glowing%20coral%20reefs,%20colorful%20fish,%20and%20mysterious%20sea%20creatures.%20The%20water%20is%20crystal%20clear,%20with%20beams%20of%20sunlight%20f.webp)
* ![A magical forest at twilight.](./media/DALL·E%202024-05-18%2010.49.27%20-%20A%20magical%20forest%20at%20twilight,%20filled%20with%20glowing%20plants,%20mystical%20creatures,%20and%20a%20sparkling%20river.%20The%20trees%20are%20tall%20and%20ancient,%20with%20bioluminesce.webp)
* ![A bustling fantasy marketplace.](./media/ae29b058-6fee-45cc-a93d-9d94f127f91c.webp)

### without list 

![calib 1](./media/DALL·E%202024-05-18%2011.38.53%20-%20An%20intricate%20and%20creative%20design%20in%20CMYK%20colors,%20incorporating%20a%20hidden%20color%20calibration%20pattern.%20The%20design%20should%20be%20complex%20and%20detailed,%20using%20ab.webp)

![calib 2](./media/DALL·E%202024-05-18%2011.39.36%20-%20An%20intricate%20and%20creative%20design%20in%20CMYK%20colors,%20incorporating%20a%20hidden%20color%20calibration%20pattern.%20The%20design%20should%20be%20complex%20and%20detailed,%20using%20ab.webp)


![calib 3](./media/DALL·E%202024-05-18%2011.39.43%20-%20An%20intricate%20and%20creative%20design%20in%20CMYK%20colors,%20incorporating%20a%20hidden%20color%20calibration%20pattern.%20The%20design%20should%20be%20complex%20and%20detailed,%20using%20ab.webp)

![calib 4](./media/DALL·E%202024-05-18%2011.39.51%20-%20An%20intricate%20and%20creative%20design%20in%20CMYK%20colors,%20incorporating%20a%20hidden%20color%20calibration%20pattern.%20The%20design%20should%20be%20complex%20and%20detailed,%20using%20ab.webp)

### in a table

| IMG A    | IMG B |
| -------- | ------- |
| ![A vibrant cyberpunk cityscape at night.](./media/DALL·E%202024-05-18%2011.43.04%20-%20A%20cyberpunk%20city%20at%20night%20with%20neon%20lights,%20tall%20skyscrapers,%20and%20a%20bustling%20street%20filled%20with%20people%20in%20futuristic%20attire.%20The%20buildings%20are%20adorned.webp)   | ![A cyberpunk alleyway illuminated by neon signs.](./media/DALL·E%202024-05-18%2011.43.21%20-%20A%20cyberpunk%20alleyway%20at%20night,%20illuminated%20by%20neon%20signs%20and%20holographic%20advertisements.%20The%20walls%20are%20covered%20with%20graffiti,%20and%20the%20ground%20is%20wet,%20r.webp)    |
| ![A bustling cyberpunk market scene.](./media/DALL·E%202024-05-18%2011.43.30%20-%20A%20cyberpunk%20market%20scene%20at%20night%20with%20stalls%20selling%20futuristic%20gadgets%20and%20neon%20signs%20everywhere.%20The%20market%20is%20crowded%20with%20people%20wearing%20cybernet.webp)  |  ![A cyberpunk rooftop scene with neon glows.](./media/DALL·E%202024-05-18%2011.43.37%20-%20A%20cyberpunk%20rooftop%20scene%20at%20night,%20with%20neon%20lights%20from%20nearby%20buildings%20casting%20colorful%20glows.%20The%20rooftops%20are%20filled%20with%20antennas,%20satellite%20di.webp)    |
